#!/bin/bash

set -xeuo pipefail

for i in {0..5}
do
    if [ ! -b /dev/loop${i} ]; then
        mknod -m 0660 /dev/loop${i} b 7 ${i}
    fi
done

chown root:disk /dev/loop*
