#!/bin/bash -e
NEW_SPLASH="${STAGE_DIR}/02-configure-plymouth/files/splash.png"

rm -f "${ROOTFS_DIR}/usr/share/plymouth/themes/pix/splash.png"

install -m 644 "$NEW_SPLASH" "${ROOTFS_DIR}/usr/share/plymouth/themes/pix/splash.png"

on_chroot << EOF
plymouth-set-default-theme pix
update-initramfs -u
EOF

